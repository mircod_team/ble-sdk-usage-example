package com.mircod.mircodandroidsdk;

import android.app.Application;

import com.mircod.mircodandroidsdk.di.aplication.AppComponent;
import com.mircod.mircodandroidsdk.di.aplication.AppModule;
import com.mircod.mircodandroidsdk.di.aplication.DaggerAppComponent;

/**
 * Created by Andrey on 20.03.2018.
 */

public class MircodBleSdkApp extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppComponent = prepareAppComponent()
                .build();
    }

    private DaggerAppComponent.Builder prepareAppComponent() {
        return DaggerAppComponent.builder().appModule(new AppModule(this));
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public AppComponent applicationComponent() {
        return sAppComponent;
    }

}
