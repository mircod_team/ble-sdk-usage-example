package com.mircod.mircodandroidsdk.ui.connection_screen.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.ui.ICharacteristicView;
import com.mircod.mircodandroidsdk.ui.connection_screen.presenter.DeviceConnectPresenter;
import com.mircod.mircodandroidsdk.ui.search_device.adapter.ble_services.ServicesListRecyclerAdapter;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.entities.BleConnectionState;

import java.util.List;
import java.util.UUID;

import rx.functions.Action1;

/**
 * Created by Andrey on 22.03.2018.
 */

public class DeviceConnectActivity extends MvpAppCompatActivity implements IDeviceConnectView {

    private static final String TAG = DeviceConnectActivity.class.getSimpleName();
    @InjectPresenter
    DeviceConnectPresenter mConnectPresenter;
    private TextView mDeviceConnectionState;
    private RecyclerView mDeviceServiceRecyclerView;
    private ServicesListRecyclerAdapter mServicesListRecyclerAdapter;
    private String mDeviceMacAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_connect_view);

        mDeviceMacAddress = getIntent().getStringExtra("deviceMacAddress");

        mDeviceConnectionState = findViewById(R.id.deviceConnectionState);
        mDeviceServiceRecyclerView = findViewById(R.id.deviceServicesRecyclerView);
        mServicesListRecyclerAdapter = new ServicesListRecyclerAdapter();
        mDeviceServiceRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDeviceServiceRecyclerView.setAdapter(mServicesListRecyclerAdapter);

        findViewById(R.id.deviceConnectBtn)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        BleDevice bleDevice = mConnectPresenter.getBleDevice(mDeviceMacAddress);
                        mConnectPresenter.connect(bleDevice);
                    }
                });

        if (mConnectPresenter.deviceIsConnected(mDeviceMacAddress)) {
            mConnectPresenter.getConnectionState(mDeviceMacAddress);
            mConnectPresenter.getAllService();
        }

        mServicesListRecyclerAdapter.getServiceClickObs()
                .subscribe(new Action1<BleDeviceService>() {
                    @Override
                    public void call(BleDeviceService bleDeviceService) {
                        mConnectPresenter.moveToCharacteristicActivity(bleDeviceService);
                    }
                });

    }

    @Override
    public void showConnectionState(BleConnectionState bleConnectionState) {
        mDeviceConnectionState.setText(bleConnectionState.toString());
    }

    @Override
    public void showAllServices(List<BleDeviceService> bleDeviceServices) {
        mServicesListRecyclerAdapter.addServicesList(bleDeviceServices);
    }

    @Override
    public void showCharacteristicsActivity(Class<? extends ICharacteristicView> characteristicViewerActivity, UUID serviceUUID) {
        Log.d(TAG, "showCharacteristicsActivity: ");
        Intent intent = new Intent(this, characteristicViewerActivity);
        intent.putExtra("deviceMac", mDeviceMacAddress);
        intent.putExtra("serviceUUID", serviceUUID);
        startActivity(intent);
    }
}
