package com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.ui;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;

import java.util.List;
import java.util.UUID;

/**
 * Created by Andrey on 27.03.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ICharacteristicView extends MvpView{

    @StateStrategyType(SkipStrategy.class)
    void showAllCharacteristics(List<IBleDeviceCharacteristic> bleDeviceServices);

    void showServiceInfo(BleDeviceService bleDeviceService);

    void showCharacteristicVal(String data, UUID characteristicUUID);
}
