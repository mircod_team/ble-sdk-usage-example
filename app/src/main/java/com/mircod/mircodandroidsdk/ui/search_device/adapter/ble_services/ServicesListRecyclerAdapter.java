package com.mircod.mircodandroidsdk.ui.search_device.adapter.ble_services;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Andrey on 21.02.2018.
 */

public class ServicesListRecyclerAdapter extends RecyclerView.Adapter<ServicesListRecyclerAdapter.MyViewHolder> {

    private List<BleDeviceService> mBleDeviceServices;
    private PublishSubject<BleDeviceService> mBleDeviceServicePublishSubject = PublishSubject.create();

    public ServicesListRecyclerAdapter() {
        mBleDeviceServices = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ServicesListRecyclerAdapter.MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.service_list_item, parent, false));

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final BleDeviceService bleDeviceService = mBleDeviceServices.get(position);
        holder.mServiceNameTextView.setText(bleDeviceService.getName());
        holder.mServiceUUIDTextView.setText(bleDeviceService.getServiceUUID().toString());

        holder.mServiceNameTextView.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBleDeviceServicePublishSubject.onNext(bleDeviceService);
            }
        });

    }

    public Observable<BleDeviceService> getServiceClickObs(){
        return mBleDeviceServicePublishSubject.asObservable();
    }

    public void addServicesList(List<BleDeviceService> bleDeviceService) {
        mBleDeviceServices = new ArrayList<>(bleDeviceService);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mBleDeviceServices.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView mServiceNameTextView;
        private TextView mServiceUUIDTextView;

        public MyViewHolder(View itemView) {
            super(itemView);
            mServiceNameTextView = itemView.findViewById(R.id.serviceName);
            mServiceUUIDTextView = itemView.findViewById(R.id.serviceUUID);
        }
    }
}
