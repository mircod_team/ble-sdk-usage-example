package com.mircod.mircodandroidsdk.ui.characteristic_viewer;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Andrey on 13.03.2018.
 */

public class CharacteristicListAdapter extends BaseAdapter {

    @NonNull
    private final Context mContext;
    private List<BleDeviceCharacteristic> mBleDeviceCharacteristics = new ArrayList<>();
    private LayoutInflater mInflater;
    private PublishSubject<BleDeviceCharacteristic> mCharacteristicClickObs = PublishSubject.create();
    private Set<Integer> mClickedItems = new HashSet<>();


    public CharacteristicListAdapter(Context context){
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View rootView, @NonNull ViewGroup parent) {
        if (rootView == null){
            rootView = mInflater.inflate(R.layout.characteristic_recycler_item, parent, false);
        }
        final BleDeviceCharacteristic characteristic = mBleDeviceCharacteristics.get(position);

        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCharacteristicClickObs.onNext(characteristic);
            }
        });

        TextView charactUUIDTextView = rootView.findViewById(R.id.characteristicUUID);

        charactUUIDTextView.setText(characteristic.getUUID().toString());
        return rootView;
    }

    public Observable<BleDeviceCharacteristic> getCharacteristicClickObs(){
        return mCharacteristicClickObs.asObservable();
    }

    @Override
    public int getCount() {
        return mBleDeviceCharacteristics.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public void addData(List<BleDeviceCharacteristic> bleDeviceCharacteristics){
        mBleDeviceCharacteristics.addAll(bleDeviceCharacteristics);
        notifyDataSetChanged();
    }

    public void updateCharacteristic(BleDeviceCharacteristic bleDeviceCharacteristic) {
        if (mBleDeviceCharacteristics.contains(bleDeviceCharacteristic)){
            mClickedItems.add(mBleDeviceCharacteristics.indexOf(bleDeviceCharacteristic));
            notifyDataSetChanged();
        }
    }
}
