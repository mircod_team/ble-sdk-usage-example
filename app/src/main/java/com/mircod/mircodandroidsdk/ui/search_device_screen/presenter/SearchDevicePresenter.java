package com.mircod.mircodandroidsdk.ui.search_device_screen.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mircod.mircodandroidsdk.MircodBleSdkApp;
import com.mircod.mircodandroidsdk.business.search_device.ISearchDeviceInteractor;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodandroidsdk.di.device_search.DeviceSearchModule;
import com.mircod.mircodandroidsdk.ui.connection_screen.view.DeviceConnectActivity;
import com.mircod.mircodandroidsdk.ui.search_device_screen.view.ISearchDeviceView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by Andrey on 20.03.2018.
 */
@InjectViewState
public class SearchDevicePresenter extends MvpPresenter<ISearchDeviceView> {

    @Inject
    ISearchDeviceInteractor mSearchDeviceInteractor;

    public SearchDevicePresenter() {
        System.out.println("SearchDevicePresenter created");
        MircodBleSdkApp.getAppComponent().plus(new DeviceSearchModule()).inject(this);
    }

    public void startScanDevices(int interval, TimeUnit timeUnit) {
        mSearchDeviceInteractor.startSearchDevices(interval, timeUnit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BleDevice>() {
                    @Override
                    public void call(BleDevice bleDevice) {
                        getViewState().showDevice(bleDevice);
                        getViewState().hideDeviceSearchBtn();
                    }
                });
    }

    public void connectToDeviceAndChangeScreen(BleDevice bleDevice) {
        mSearchDeviceInteractor.connectToDevice(bleDevice);
        getViewState().moveToConnectionScreen(DeviceConnectActivity.class, bleDevice.getMacAddress());
    }

}
