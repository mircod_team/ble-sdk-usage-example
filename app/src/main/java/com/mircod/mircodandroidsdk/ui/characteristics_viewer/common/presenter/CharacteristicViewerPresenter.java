package com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.presenter;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mircod.mircodandroidsdk.MircodBleSdkApp;
import com.mircod.mircodandroidsdk.business.device_characteristic.IDeviceCharacteristicInteractor;
import com.mircod.mircodandroidsdk.di.device_characteristic.DeviceCharacteristicModule;
import com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.ui.ICharacteristicView;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;
import com.mircod.mircodblesdk.utils.BytesConverterHelper;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by Andrey on 27.03.2018.
 */
@InjectViewState
public class CharacteristicViewerPresenter extends MvpPresenter<ICharacteristicView> {

    @Inject
    IDeviceCharacteristicInteractor mCharacteristicInteractor;

    public CharacteristicViewerPresenter() {
        MircodBleSdkApp.getAppComponent()
                .plus(new DeviceCharacteristicModule())
                .inject(this);
    }

    public void setDeviceMacAddress(String macAddress) {
        mCharacteristicInteractor.setDeviceMacAddress(macAddress);
    }

    public void getCharacteristicsList(UUID serviceUUID) {
        mCharacteristicInteractor.getServiceCharacteristics(serviceUUID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<IBleDeviceCharacteristic>>() {
                    @Override
                    public void call(List<IBleDeviceCharacteristic> bleDeviceCharacteristics) {
                        getViewState().showAllCharacteristics(bleDeviceCharacteristics);
                    }
                });
    }

    public void getDeviceServiceInfo(UUID serviceUUID) {
        mCharacteristicInteractor.getDeviceServiceByUUID(serviceUUID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BleDeviceService>() {
                    @Override
                    public void call(BleDeviceService bleDeviceService) {
                        getViewState().showServiceInfo(bleDeviceService);
                    }
                });
    }

    public void readCharacteristic(final IBleDeviceCharacteristic characteristic) {
        mCharacteristicInteractor.readCharacteristic(characteristic)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<IBleDeviceCharacteristic>() {
                    @Override
                    public void call(IBleDeviceCharacteristic bleDeviceCharacteristic) {
                        getViewState().showCharacteristicVal(BytesConverterHelper.bytesToHex(bleDeviceCharacteristic.getData()), characteristic.getUUID());
                    }
                });
    }

    public void notifyCharacteristic(final IBleDeviceCharacteristic characteristic) {
        mCharacteristicInteractor.notifyCharacteristic(characteristic)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<IBleDeviceCharacteristic>() {
                    @Override
                    public void call(IBleDeviceCharacteristic bleDeviceCharacteristic) {
                        getViewState().showCharacteristicVal(BytesConverterHelper.bytesToHex(bleDeviceCharacteristic.getData()), characteristic.getUUID());
                    }
                });
    }

    public void writeCharacteristicToDevice(IBleDeviceCharacteristic characteristic, byte[] data) {
        mCharacteristicInteractor.writeCharacteristic(characteristic, data)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<byte[]>() {
                    @Override
                    public void call(byte[] bytes) {
                        Log.d("CharactViewerPresenter", Arrays.toString(bytes));
                    }
                });
    }
}
