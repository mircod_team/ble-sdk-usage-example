package com.mircod.mircodandroidsdk.ui.connection_screen.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.ui.ICharacteristicView;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.entities.BleConnectionState;

import java.util.List;
import java.util.UUID;

/**
 * Created by Andrey on 23.03.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface IDeviceConnectView extends MvpView {
    void showConnectionState(BleConnectionState bleConnectionState);

    @StateStrategyType(AddToEndStrategy.class)
    void showAllServices(List<BleDeviceService> bleDeviceServices);

    @StateStrategyType(SkipStrategy.class)
    void showCharacteristicsActivity(Class<? extends ICharacteristicView> characteristicViewerActivityClass, UUID characteristicUUID);

}
