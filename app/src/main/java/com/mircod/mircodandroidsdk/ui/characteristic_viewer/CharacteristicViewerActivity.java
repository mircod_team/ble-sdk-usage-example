package com.mircod.mircodandroidsdk.ui.characteristic_viewer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodandroidsdk.ui.search_device.fmts.ServicesListFragment;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleCharacteristicProperty;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceDescriptor;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by Andrey on 22.02.2018.
 */

public class CharacteristicViewerActivity extends AppCompatActivity {

    BleDeviceService mBleDeviceService;
    private CharacteristicRecyclerAdapter mCharacteristicRecyclerAdapter;
    private final String TAG = "CharacteristicViewerA";
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characteristic_viewer);
        mBleDeviceService = ServicesListFragment.getBleDeviceService();

        ((TextView) findViewById(R.id.serviceName)).setText(mBleDeviceService.getName());
        ((TextView) findViewById(R.id.serviceUUID)).setText("UUID: ".concat(mBleDeviceService.getServiceUUID().toString()));

        showCharacteristics(mBleDeviceService.getBleDeviceCharacteristics());

        UUID[] uuids = new UUID[]{UUID.fromString("0000ffe2-0000-1000-8000-00805f9b34fb")};
    }

    private void showCharacteristics(List<IBleDeviceCharacteristic> bleDeviceCharacteristics) {
        LinearLayout rootView = findViewById(R.id.characteristicsRoot);

//        ServicesListFragment.sBleDeviceConnection
//                .mircodCharacteristicSubscribe(characteristic1, new ShortParser(), new CastFunc<MircodCharacteristicData, ShortData>() {
//                    @Override
//                    public ShortData call(MircodCharacteristicData mircodCharacteristicData) {
//                        return (ShortData) mircodCharacteristicData;
//                    }
//                })
//                .map(new Func1<ShortData, ShortData>() {
//                    @Override
//                    public ShortData call(ShortData shortData) {
//                        Log.d(TAG, shortData.toString());
//                        return shortData;
//                    }
//                }).subscribe();

        for (final IBleDeviceCharacteristic characteristic : bleDeviceCharacteristics) {
            View innerView = getLayoutInflater().inflate(R.layout.characteristic_recycler_item, null);
            final TextView characteristicUUID = innerView.findViewById(R.id.characteristicUUID);
            final TextView characteristicProperty = innerView.findViewById(R.id.characteristicProperties);
            final TextView characteristicName = innerView.findViewById(R.id.characteristicName);
            final View characteristicEnable = innerView.findViewById(R.id.characteristicWrite);
            final View characteristicDisable = innerView.findViewById(R.id.characteristicRead);

            characteristicUUID.setText(characteristic.getUUID().toString());
            characteristicName.setText(characteristic.getName());

            StringBuilder characteristicPropStringBuilder = new StringBuilder();

            int delimiterIter = 0;

            Set<BleCharacteristicProperty> properties = characteristic.getProperties();
            for (BleCharacteristicProperty characteristicProp : properties) {
                if (delimiterIter > 0){
                    characteristicPropStringBuilder.append(", ");
                }
                characteristicPropStringBuilder.append(characteristicProp);
                delimiterIter++;
            }

            characteristicProperty.setText(characteristicPropStringBuilder.toString());

            List<BleDeviceDescriptor> bleDeviceDescriptors = characteristic.getBleDeviceDescriptors();

            if (bleDeviceDescriptors.size() != 0) {
                innerView.findViewById(R.id.descriptorsDescription).setVisibility(View.VISIBLE);
                showCharacteristicDescriptors(bleDeviceDescriptors, (LinearLayout) innerView.findViewById(R.id.descriptorsRoot));
            }

            rootView.addView(innerView);

        }
    }


    private void subscribeToCharacteristic(final BleDeviceCharacteristic characteristic, final TextView characteristicValView) {
        ServicesListFragment.sBleDeviceConnection
                .subscribeCharacteristicNotification(characteristic)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<IBleDeviceCharacteristic>() {
                    @Override
                    public void call(IBleDeviceCharacteristic bleDeviceCharacteristic) {
                        characteristicValView.setText(Arrays.toString(bleDeviceCharacteristic.getData()));
                    }
                });
    }

    private void unsubscribeFromCharacteristic(final BleDeviceCharacteristic characteristic, final TextView characteristicValView) {
        ServicesListFragment.sBleDeviceConnection
                .unsubscribeCharacteristicNotification(characteristic.getUUID())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<byte[]>() {
                    @Override
                    public void call(byte[] bytes) {
                        characteristicValView.setText(Arrays.toString(bytes));
                    }
                });
    }

    private void showCharacteristicDescriptors(List<BleDeviceDescriptor> bleDeviceDescriptor, ViewGroup rootView) {

        for (BleDeviceDescriptor deviceDescriptor : bleDeviceDescriptor) {
            View descriptorsView = getLayoutInflater().inflate(R.layout.descriptor_view, null);
            ((TextView) descriptorsView.findViewById(R.id.descriptorName)).setText(deviceDescriptor.getName());
            ((TextView) descriptorsView.findViewById(R.id.descriptorUUIDVal)).setText(deviceDescriptor.getUUID().toString());
            rootView.addView(descriptorsView);
        }

    }

}
