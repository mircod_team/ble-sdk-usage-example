package com.mircod.mircodandroidsdk.ui.search_device.fmts;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodandroidsdk.ui.characteristic_viewer.CharacteristicViewerActivity;
import com.mircod.mircodandroidsdk.ui.search_device.adapter.ble_services.ServicesListRecyclerAdapter;
import com.mircod.mircodblesdk.core.IBleDeviceConnection;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceServices;
import com.mircod.mircodblesdk.entities.BleConnectionState;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;


import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by Andrey on 21.02.2018.
 */

public class ServicesListFragment extends Fragment {

    private View mRootView;

    private ServicesListRecyclerAdapter mServicesListRecyclerAdapter;
    private IBleDevice mBleDevice;
    private final String TAG = "ServicesListFragment";
    private Subscription mSubscription;
    private static BleDeviceService mBleDeviceService;
    public static IBleDeviceConnection sBleDeviceConnection;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = this.getArguments();
        if (arguments != null) {
            mBleDevice = arguments.getParcelable("device");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.service_list_fmt, container, false);
        RecyclerView servicesRecyclerView = mRootView.findViewById(R.id.servicesListRecyclerView);
        mServicesListRecyclerAdapter = new ServicesListRecyclerAdapter();
        servicesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        servicesRecyclerView.setAdapter(mServicesListRecyclerAdapter);

        mSubscription = mBleDevice.observeConnectionState()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BleConnectionState>() {
                    @Override
                    public void call(BleConnectionState bleConnectionState) {
                        Log.d(TAG, "BleConnectionState: " + bleConnectionState);
                        if (bleConnectionState == BleConnectionState.CONNECTED) {
                            System.out.println();
                            unsubscribe();
                        }
                        if (bleConnectionState == BleConnectionState.DISCONNECTED) {
                            unsubscribe();
                        }
                    }
                });

//        mBleDevice
//                .connect()
//                .subscribe(new Action1<IBleDeviceConnection>() {
//                    @Override
//                    public void call(IBleDeviceConnection bleDeviceConnection) {
//                        startReadData(bleDeviceConnection);
//                        Log.d(TAG, "BleDeviceConnection");
//                    }
//                });

        mServicesListRecyclerAdapter.getServiceClickObs()
                .subscribe(new Action1<BleDeviceService>() {
                    @Override
                    public void call(BleDeviceService bleDeviceService) {
                        mBleDeviceService = bleDeviceService;
                        Intent intent = new Intent(getContext(), CharacteristicViewerActivity.class);
                        startActivity(intent);
                    }
                });
        return mRootView;
    }

    public static BleDeviceService getBleDeviceService() {
        return mBleDeviceService;
    }

    private void startReadData(final IBleDeviceConnection bleDeviceConnection) {
        sBleDeviceConnection = bleDeviceConnection;

        bleDeviceConnection.getBleDeviceServices()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BleDeviceServices>() {
                    @Override
                    public void call(BleDeviceServices bleDeviceServices) {
                        mServicesListRecyclerAdapter.addServicesList(bleDeviceServices.getDeviceServices());
                    }
                });

    }

    private void unsubscribe() {
        mSubscription.unsubscribe();
    }


}
