package com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.presenter.CharacteristicViewerPresenter;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleCharacteristicProperty;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceDescriptor;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

/**
 * Created by Andrey on 27.03.2018.
 */

public class CharacteristicsViewerActivity extends MvpAppCompatActivity implements ICharacteristicView {

    @InjectPresenter
    CharacteristicViewerPresenter mCharacteristicViewerPresenter;
    private String mDeviceMac;
    private LinearLayout mCharacteristicsRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characteristic_viewer);

        mDeviceMac = getIntent().getStringExtra("deviceMac");
        UUID deviceUUID = (UUID) getIntent().getSerializableExtra("serviceUUID");

        mCharacteristicViewerPresenter.setDeviceMacAddress(mDeviceMac);
        mCharacteristicViewerPresenter.getCharacteristicsList(deviceUUID);
        mCharacteristicViewerPresenter.getDeviceServiceInfo(deviceUUID);
    }

    @Override
    public void showAllCharacteristics(List<IBleDeviceCharacteristic> bleDeviceCharacteristics) {
        showServiceCharacteristics(bleDeviceCharacteristics);
    }

    @Override
    public void showServiceInfo(BleDeviceService bleDeviceService) {
        ((TextView) findViewById(R.id.serviceUUID)).setText(bleDeviceService.getServiceUUID().toString());
        ((TextView) findViewById(R.id.serviceName)).setText(bleDeviceService.getName());
    }

    private void showServiceCharacteristics(List<IBleDeviceCharacteristic> bleDeviceCharacteristics) {
        mCharacteristicsRootView = findViewById(R.id.characteristicsRoot);
        for (final IBleDeviceCharacteristic characteristic : bleDeviceCharacteristics) {
            View innerView = getLayoutInflater().inflate(R.layout.characteristic_recycler_item, null);
            final TextView charactUUIDTextView = innerView.findViewById(R.id.characteristicUUID);
            final TextView charactValTextView = innerView.findViewById(R.id.characteristicVal);
            Log.d("CharacteristicsViewer", "id: " + charactValTextView.getId() + " hashCode: " + charactValTextView.hashCode());
            charactValTextView.setTag(characteristic.getUUID());
            final TextView charactPropertyTextView = innerView.findViewById(R.id.characteristicProperties);
            final TextView charactNameTextView = innerView.findViewById(R.id.characteristicName);

            charactUUIDTextView.setText(characteristic.getUUID().toString());
            charactNameTextView.setText(characteristic.getName());

            showCharacteristicProperties(characteristic, charactPropertyTextView, innerView, charactValTextView);

            List<BleDeviceDescriptor> bleDeviceDescriptors = characteristic.getBleDeviceDescriptors();

            if (bleDeviceDescriptors.size() != 0) {
                innerView.findViewById(R.id.descriptorsDescription).setVisibility(View.VISIBLE);
                showCharacteristicDescriptors(bleDeviceDescriptors, (LinearLayout) innerView.findViewById(R.id.descriptorsRoot));
            }

            mCharacteristicsRootView.addView(innerView);
        }
    }

    private void showCharacteristicProperties(IBleDeviceCharacteristic bleDeviceCharacteristic, TextView propTextView, View innerView, TextView charactValTextView) {
        Set<BleCharacteristicProperty> bleCharacteristicProperties = bleDeviceCharacteristic.getProperties();
        int delimiterIter = 0;
        StringBuilder characteristicPropStringBuilder = new StringBuilder();

        for (BleCharacteristicProperty characteristicProp : bleCharacteristicProperties) {
            if (delimiterIter > 0) {
                characteristicPropStringBuilder.append(", ");
            }

            switch (characteristicProp) {
                case READ: {
                    View readAction = innerView.findViewById(R.id.characteristicRead);
                    readAction.setVisibility(View.VISIBLE);
                    readAction.setOnClickListener(readCharacteristicClickListener(bleDeviceCharacteristic));
                    break;
                }
                case WRITE: {
                    View writeAction = innerView.findViewById(R.id.characteristicWrite);
                    writeAction.setVisibility(View.VISIBLE);
                    writeAction.setOnClickListener(writeCharacteristicToDevice(bleDeviceCharacteristic));
                    break;
                }
                case NOTIFY: {
                    View notifyAction = innerView.findViewById(R.id.characteristicNotification);
                    notifyAction.setVisibility(View.VISIBLE);
                    notifyAction.setOnClickListener(notifyCharacteristicClickListener(bleDeviceCharacteristic));
                    break;
                }
            }
            characteristicPropStringBuilder.append(characteristicProp);
            delimiterIter++;
        }
        propTextView.setText(characteristicPropStringBuilder.toString());
    }

    private View.OnClickListener readCharacteristicClickListener(final IBleDeviceCharacteristic characteristic) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCharacteristicViewerPresenter.readCharacteristic(characteristic);
            }
        };
    }

    private View.OnClickListener notifyCharacteristicClickListener(final IBleDeviceCharacteristic characteristic) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCharacteristicViewerPresenter.notifyCharacteristic(characteristic);
            }
        };
    }

    private View.OnClickListener writeCharacteristicToDevice(final IBleDeviceCharacteristic characteristic) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                byte[] bytes = new byte[5];
                new Random().nextBytes(bytes);
                mCharacteristicViewerPresenter.writeCharacteristicToDevice(characteristic, bytes);
            }
        };
    }

    private void showCharacteristicDescriptors(List<BleDeviceDescriptor> bleDeviceDescriptor, ViewGroup rootView) {
        for (BleDeviceDescriptor deviceDescriptor : bleDeviceDescriptor) {
            View descriptorsView = getLayoutInflater().inflate(R.layout.descriptor_view, null);
            ((TextView) descriptorsView.findViewById(R.id.descriptorName)).setText(deviceDescriptor.getName());
            ((TextView) descriptorsView.findViewById(R.id.descriptorUUIDVal)).setText(deviceDescriptor.getUUID().toString());
            rootView.addView(descriptorsView);
        }
    }

    @Override
    public void showCharacteristicVal(String data, UUID characteristicUUID) {
        if (mCharacteristicsRootView != null){
            TextView viewWithTag = mCharacteristicsRootView.findViewWithTag(characteristicUUID);
            if (viewWithTag != null) {
                viewWithTag.setText(data);
            }
        }
    }
}
