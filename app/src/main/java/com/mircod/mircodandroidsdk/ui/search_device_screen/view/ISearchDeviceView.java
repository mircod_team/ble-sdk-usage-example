package com.mircod.mircodandroidsdk.ui.search_device_screen.view;

import android.support.annotation.NonNull;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodandroidsdk.ui.connection_screen.view.IDeviceConnectView;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Andrey on 20.03.2018.
 */
@StateStrategyType(AddToEndSingleStrategy.class)
public interface ISearchDeviceView extends MvpView {

    void showMessage(String message);

    @StateStrategyType(AddToEndStrategy.class)
    void showDevice(BleDevice bleDevice);

    void hideDeviceSearchBtn();

    void showDeviceSearchBtn();

    @StateStrategyType(SkipStrategy.class)
    void moveToConnectionScreen(@NonNull Class<? extends IDeviceConnectView> anotherActivity, @NotNull String deviceMacAddress);
}
