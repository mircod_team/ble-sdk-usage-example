package com.mircod.mircodandroidsdk.ui.connection_screen.presenter;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mircod.mircodandroidsdk.MircodBleSdkApp;
import com.mircod.mircodandroidsdk.business.device_connect.IDeviceConnectInteractor;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodandroidsdk.di.device_connection.DeviceConnectModule;
import com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.ui.CharacteristicsViewerActivity;
import com.mircod.mircodandroidsdk.ui.characteristics_viewer.ecg.ui.EcgCharacteristicsViewerActivity;
import com.mircod.mircodandroidsdk.ui.connection_screen.view.IDeviceConnectView;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.entities.BleConnectionState;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Andrey on 23.03.2018.
 */
@InjectViewState
public class DeviceConnectPresenter extends MvpPresenter<IDeviceConnectView> {

    private static final String TAG = DeviceConnectPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription = new CompositeSubscription();
    @Inject
    IDeviceConnectInteractor mConnectInteractor;

    public DeviceConnectPresenter() {
        Log.d(TAG, "DeviceConnectPresenter: ");
        MircodBleSdkApp.getAppComponent().plus(new DeviceConnectModule()).inject(this);
    }

    public BleDevice getBleDevice(@NotNull String deviceMacAddress) {
        return mConnectInteractor.getBleDevice(deviceMacAddress);
    }

    public void connect(BleDevice bleDevice) {
        Subscription subscribe = mConnectInteractor.connect(bleDevice)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BleConnectionState>() {
                    @Override
                    public void call(BleConnectionState bleConnectionState) {
                        getViewState().showConnectionState(bleConnectionState);
                        Log.d(TAG, "connectionState = " + bleConnectionState);
                        if (bleConnectionState == BleConnectionState.CONNECTED) {
                            getAllService();
                        }
                    }
                });
        mCompositeSubscription.add(subscribe);
    }

    public void getAllService() {
        mConnectInteractor.getAllServices()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<BleDeviceService>>() {
                    @Override
                    public void call(List<BleDeviceService> bleDeviceServices) {
                        getViewState().showAllServices(bleDeviceServices);
                    }
                });
    }

    public void getConnectionState(String macAddress) {
        mConnectInteractor.getDeviceConnectionState(macAddress)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BleConnectionState>() {
                    @Override
                    public void call(BleConnectionState bleConnectionState) {
                        getViewState().showConnectionState(bleConnectionState);
                    }
                });
    }

    public boolean deviceIsConnected(String macAddress) {
        return mConnectInteractor.deviceIsConnected(macAddress);
    }

    public void moveToCharacteristicActivity(BleDeviceService bleDeviceService) {
        if (!bleDeviceService.getServiceUUID().toString().equals(UUID.fromString("0000ffe0-0000-1000-8000-00805f9b34fb").toString()))
            getViewState().showCharacteristicsActivity(CharacteristicsViewerActivity.class, bleDeviceService.getServiceUUID());
        else
            getViewState().showCharacteristicsActivity(EcgCharacteristicsViewerActivity.class, bleDeviceService.getServiceUUID());
    }
}