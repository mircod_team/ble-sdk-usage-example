package com.mircod.mircodandroidsdk.ui.search_device_screen.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodandroidsdk.ui.connection_screen.view.IDeviceConnectView;
import com.mircod.mircodandroidsdk.ui.search_device_screen.presenter.SearchDevicePresenter;
import com.mircod.mircodandroidsdk.ui.search_device_screen.view.adapter.SearchDeviceAdapter;

import org.jetbrains.annotations.NotNull;

import rx.functions.Action1;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by Andrey on 20.03.2018.
 */

public class SearchDeviceActivity extends MvpAppCompatActivity implements ISearchDeviceView {

    @InjectPresenter
    SearchDevicePresenter mSearchDevicePresenter;
    private RecyclerView mDeviceRecyclerView;
    private SearchDeviceAdapter mSearchDeviceAdapter;
    private ImageView mDeviceSearchImg;
    private String TAG = SearchDeviceActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_device_view);

        mDeviceRecyclerView = findViewById(R.id.bleDevices);
        mDeviceSearchImg = findViewById(R.id.startScanBtn);
        mSearchDeviceAdapter = new SearchDeviceAdapter();
        mDeviceRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mDeviceRecyclerView.setAdapter(mSearchDeviceAdapter);

        mDeviceSearchImg
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSearchDevicePresenter.startScanDevices(3, SECONDS);
                    }
                });

        mSearchDeviceAdapter.getDeviceClickObs()
                .subscribe(new Action1<BleDevice>() {
                    @Override
                    public void call(final BleDevice bleDevice) {
                        mSearchDevicePresenter.connectToDeviceAndChangeScreen(bleDevice);
                    }
                });

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDevice(BleDevice bleDevice) {
        mSearchDeviceAdapter.addBleDevice(bleDevice);
    }

    @Override
    public void hideDeviceSearchBtn() {
        mDeviceSearchImg.setVisibility(GONE);
    }

    @Override
    public void showDeviceSearchBtn() {
        mDeviceSearchImg.setVisibility(VISIBLE);
    }

    @Override
    public void moveToConnectionScreen(@NotNull Class<? extends IDeviceConnectView> anotherActivity, @NotNull String deviceMacAddress) {
        Log.d(TAG, "moveToConnectionScreen: ");
        Intent intent = new Intent(this, anotherActivity);
        intent.putExtra("deviceMacAddress", deviceMacAddress);
        startActivity(intent);
    }
}
