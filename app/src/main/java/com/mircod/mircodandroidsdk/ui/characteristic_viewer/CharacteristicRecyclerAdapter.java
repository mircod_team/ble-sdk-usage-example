package com.mircod.mircodandroidsdk.ui.characteristic_viewer;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleCharacteristicProperty;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceDescriptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Andrey on 22.02.2018.
 */

public class CharacteristicRecyclerAdapter extends RecyclerView.Adapter<CharacteristicRecyclerAdapter.MyViewHolder> {

    private final LayoutInflater mLayoutInflater;
    private List<BleDeviceCharacteristic> mBleDeviceCharacteristics;
    private PublishSubject<BleDeviceCharacteristic> mCharacteristicClickObs = PublishSubject.create();
    private Set<Integer> mIntegers = new HashSet<>();

    public CharacteristicRecyclerAdapter(LayoutInflater layoutInflater) {
        mLayoutInflater = layoutInflater;
        mBleDeviceCharacteristics = new ArrayList<>();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.characteristic_recycler_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final BleDeviceCharacteristic bleDeviceCharacteristic = mBleDeviceCharacteristics.get(position);
        holder.mName.setText(bleDeviceCharacteristic.getName());
        holder.mUUIDTextView.setText(bleDeviceCharacteristic.getUUID().toString());
        StringBuilder property = new StringBuilder();
        int i = 0;

        for (BleCharacteristicProperty bleCharacteristicProperty : bleDeviceCharacteristic.getProperties()) {
            if (i > 0) {
                property.append(", ");
            }
            property.append(bleCharacteristicProperty.name());
            i++;
        }

        byte[] mCharacteristicData = bleDeviceCharacteristic.getData();

        if (mCharacteristicData != null && mIntegers.contains(position)){
            holder.mCharacteristicVal.setText(Arrays.toString(mCharacteristicData));
        }
        List<BleDeviceDescriptor> bleDeviceDescriptors = bleDeviceCharacteristic.getBleDeviceDescriptors();

        holder.mPropertyTextView.setText(property);
        holder.mRootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCharacteristicClickObs.onNext(bleDeviceCharacteristic);
            }
        });
    }

    public Observable<BleDeviceCharacteristic> getCharacteristicClickObs(){
        return mCharacteristicClickObs.asObservable();
    }

    public void addCharacteristicList(List<BleDeviceCharacteristic> characteristicList) {
        mBleDeviceCharacteristics.addAll(characteristicList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mBleDeviceCharacteristics.size();
    }

    public void updateData(BleDeviceCharacteristic bleDeviceCharacteristic) {
        if (mBleDeviceCharacteristics.contains(bleDeviceCharacteristic)) {
            int pos = mBleDeviceCharacteristics.indexOf(bleDeviceCharacteristic);
            mIntegers.add(pos);
            notifyItemChanged(pos);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mUUIDTextView;
        private TextView mPropertyTextView;
        private TextView mName;
        private LinearLayout mDescriptorView;
        private View mRootView;
        private TextView mCharacteristicVal;

        public MyViewHolder(View itemView) {
            super(itemView);
            mUUIDTextView = itemView.findViewById(R.id.characteristicUUID);
            mPropertyTextView = itemView.findViewById(R.id.characteristicProperties);
            mName = itemView.findViewById(R.id.characteristicName);
        }
    }
}
