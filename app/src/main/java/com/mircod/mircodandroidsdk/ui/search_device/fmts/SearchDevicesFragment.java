package com.mircod.mircodandroidsdk.ui.search_device.fmts;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mircod.mircodandroidsdk.MainActivity;
import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodandroidsdk.ui.search_device.adapter.DeviceListRecyclerAdapter;
import com.mircod.mircodblesdk.client.IMircodSdkClient;
import com.mircod.mircodblesdk.client.MircodSdkClient;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;
import com.mircod.mircodblesdk.entities.ble_device.IBleScanDevice;
import com.mircod.mircodblesdk.scanner.ScanSettings;

import java.util.concurrent.TimeUnit;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;


/**
 * Created by Andrey on 25.12.2017.
 */

public class SearchDevicesFragment extends Fragment {

    private View mRootView;
    private IMircodSdkClient mMircodSdkClient;
    private RecyclerView mFoundDeviceRecyclerView;
    private DeviceListRecyclerAdapter mDeviceListRecyclerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRootView = inflater
                .inflate(R.layout.search_list_fmt, container, false);

        mFoundDeviceRecyclerView = mRootView.findViewById(R.id.foundDeviceList);
        mFoundDeviceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mDeviceListRecyclerAdapter = new DeviceListRecyclerAdapter();
        mFoundDeviceRecyclerView.setAdapter(mDeviceListRecyclerAdapter);

        mMircodSdkClient = MircodSdkClient.create(getActivity().getApplicationContext());
        mMircodSdkClient.scanDevices(new ScanSettings.Builder().setScanPeriod(10).build())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<IBleScanDevice>() {
                    @Override
                    public void call(IBleScanDevice bleDevice) {
                        mDeviceListRecyclerAdapter.addItem(bleDevice);
                    }
                });

        mDeviceListRecyclerAdapter.getConnectionBtnClickObs()
                .subscribe(new Action1<IBleDevice>() {
                    @Override
                    public void call(IBleDevice bleDevice) {
                        ((MainActivity) getActivity()).connectToDevice(bleDevice);
                    }
                });

        return mRootView;
    }
}
