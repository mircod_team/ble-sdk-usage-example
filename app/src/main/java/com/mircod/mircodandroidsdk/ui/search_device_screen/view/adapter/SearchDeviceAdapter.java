package com.mircod.mircodandroidsdk.ui.search_device_screen.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * Created by Andrey on 21.03.2018.
 */

public class SearchDeviceAdapter extends RecyclerView.Adapter<SearchDeviceAdapter.ViewHolder> {

    private List<BleDevice> mBleDevices;
    private PublishSubject<BleDevice> mClickedDeviceObs = PublishSubject.create();

    public SearchDeviceAdapter() {
        mBleDevices = new ArrayList<>();
    }

    @Override
    public SearchDeviceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.search_device_item, parent, false));
    }

    @Override
    public void onBindViewHolder(SearchDeviceAdapter.ViewHolder holder, int position) {
        final BleDevice bleDevice = mBleDevices.get(position);
        holder.mDeviceName.setText(bleDevice.getName());
        holder.mDeviceMacAddress.setText(bleDevice.getMacAddress());
        holder.mDeviceBoundedState.setText(bleDevice.getBoundedState());
        holder.mDeviceUUID.setText(bleDevice.getUUIDString());
        holder.mDeviceManufacturerData.setText(bleDevice.getManufacturerData());

        holder.mRootItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClickedDeviceObs.onNext(bleDevice);
            }
        });
    }

    public void addBleDevice(BleDevice bleDevice) {
        mBleDevices.add(bleDevice);
        notifyItemInserted(mBleDevices.size() - 1);
    }

    public Observable<BleDevice> getDeviceClickObs(){
        return mClickedDeviceObs.asObservable();
    }

    @Override
    public int getItemCount() {
        return mBleDevices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mDeviceName;
        private TextView mDeviceMacAddress;
        private TextView mDeviceBoundedState;
        private TextView mDeviceUUID;
        private TextView mDeviceManufacturerData;
        private ViewGroup mRootItem;

        public ViewHolder(View itemView) {
            super(itemView);
            mDeviceName = itemView.findViewById(R.id.searchDeviceName);
            mDeviceMacAddress = itemView.findViewById(R.id.searchDeviceMac);
            mDeviceBoundedState = itemView.findViewById(R.id.searchDeviceBoundStatus);
            mDeviceUUID = itemView.findViewById(R.id.searchDeviceUUID);
            mDeviceManufacturerData = itemView.findViewById(R.id.searchDeviceManufacturerData);
            mRootItem = itemView.findViewById(R.id.searchDeviceRootItem);
        }
    }
}