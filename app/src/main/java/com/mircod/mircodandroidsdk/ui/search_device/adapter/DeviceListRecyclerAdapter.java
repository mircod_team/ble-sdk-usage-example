package com.mircod.mircodandroidsdk.ui.search_device.adapter;

import android.annotation.TargetApi;
import android.bluetooth.le.ScanRecord;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mircod.mircodandroidsdk.R;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;
import com.mircod.mircodblesdk.entities.ble_device.IBleScanDevice;
import com.mircod.mircodblesdk.utils.BytesConverterHelper;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.subjects.PublishSubject;


/**
 * Created by Andrey on 19.12.2017.
 */
@TargetApi(21)
public class DeviceListRecyclerAdapter extends RecyclerView.Adapter<DeviceListRecyclerAdapter.MyViewHolder> {

    private List<IBleScanDevice> mBleScanDevices = new ArrayList<>();
    private PublishSubject<IBleDevice> mItemClickObs = PublishSubject.create();

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.device_list_item, parent, false));
    }

    public void addItem(IBleScanDevice bleScanDevice) {
        mBleScanDevices.add(bleScanDevice);
        notifyItemInserted(mBleScanDevices.size() - 1);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final IBleScanDevice bleScanDevice = mBleScanDevices.get(position);

        holder.mDeviceName.setText(bleScanDevice.getDeviceName() == null ? "unidentified device" : bleScanDevice.getDeviceName());
        ScanRecord scanRecord = bleScanDevice.getScanRecord();
        if (scanRecord != null && scanRecord.getManufacturerSpecificData() != null) {
            byte[] bytes = scanRecord.getManufacturerSpecificData().valueAt(0);
            if (bytes != null && bytes.length != 0) {
                holder.mManufacturerSpecificVal.setText("0x".concat(BytesConverterHelper.bytesToHex(bytes)));
            }
            if (scanRecord.getServiceUuids() != null && scanRecord.getServiceUuids().size() != 0){
                holder.mServiceUUID.setText("Service UUID: ".concat(scanRecord.getServiceUuids().get(0).toString().split("-")[0]));
            }
        }else{
            holder.mManufacturerSpecificVal.setText("not found");
        }

        holder.mDeviceBoundState.setText(bleScanDevice.isBound() ? "Bounded" : "Not bounded");
        holder.mConnectionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClickObs.onNext(bleScanDevice);
            }
        });

        holder.mDeviceMac.setText(bleScanDevice.getDeviceMacAddress());
        holder.mDeviceRssi.setText("rssi: ".concat(String.valueOf(bleScanDevice.getRssi())));
    }

    public Observable<IBleDevice> getConnectionBtnClickObs() {
        return mItemClickObs.asObservable();
    }

    @Override
    public int getItemCount() {
        return mBleScanDevices.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView mDeviceName;
        private TextView mDeviceMac;
        private TextView mDeviceRssi;
        private TextView mDeviceBoundState;
        private TextView mManufacturerSpecificVal;
        private Button mConnectionButton;
        private TextView mServiceUUID;

        public MyViewHolder(View itemView) {
            super(itemView);
            mDeviceName = itemView.findViewById(R.id.listItemDeviceName);
            mDeviceMac = itemView.findViewById(R.id.listItemDeviceMac);
            mDeviceRssi = itemView.findViewById(R.id.listItemDeviceRssi);
            mManufacturerSpecificVal = itemView.findViewById(R.id.manufacturerVal);
            mDeviceBoundState = itemView.findViewById(R.id.listItemDeviceBoundState);
            mConnectionButton = itemView.findViewById(R.id.listItemDeviceConnectBtn);
            mServiceUUID = itemView.findViewById(R.id.ad_serviceUUID);
        }
    }
}
