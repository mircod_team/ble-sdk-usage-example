package com.mircod.mircodandroidsdk.ui.characteristics_viewer.ecg.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.mircod.mircodandroidsdk.MircodBleSdkApp;
import com.mircod.mircodandroidsdk.business.ecg_device_characterisitcs.IHolterCharacteristicsInteractor;
import com.mircod.mircodandroidsdk.di.holter_characteristics.EcgPatchDeviceModule;
import com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.ui.ICharacteristicView;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

@InjectViewState
public class EcgPatchCharacteristicsViewerPresenter extends MvpPresenter<ICharacteristicView> {

    @Inject
    IHolterCharacteristicsInteractor mHolterCharacteristicsInteractor;

    public EcgPatchCharacteristicsViewerPresenter() {
        MircodBleSdkApp.getAppComponent()
                .plus(new EcgPatchDeviceModule())
                .inject(this);
    }

    public void setDeviceMacAddress(String deviceMacAddress) {
        mHolterCharacteristicsInteractor.setDeviceMacAddress(deviceMacAddress);
    }

    public void getCharacteristicsList(UUID serviceUUID) {
        mHolterCharacteristicsInteractor.getServiceCharacteristics(serviceUUID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<List<IBleDeviceCharacteristic>>() {
                    @Override
                    public void call(List<IBleDeviceCharacteristic> bleDeviceCharacteristics) {
                        getViewState().showAllCharacteristics(bleDeviceCharacteristics);
                    }
                });

    }

    public void getDeviceServiceInfo(UUID serviceUUID) {
        mHolterCharacteristicsInteractor.getDeviceServiceByUUID(serviceUUID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BleDeviceService>() {
                    @Override
                    public void call(BleDeviceService bleDeviceService) {
                        getViewState().showServiceInfo(bleDeviceService);
                    }
                });
    }

    public void notifyCharacteristic(final IBleDeviceCharacteristic characteristic) {
        mHolterCharacteristicsInteractor.subscribeCharacteristic(characteristic)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        getViewState().showCharacteristicVal(s, characteristic.getUUID());
                    }
                });
    }
}
