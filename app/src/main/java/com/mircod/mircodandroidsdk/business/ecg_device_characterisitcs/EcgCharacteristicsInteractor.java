package com.mircod.mircodandroidsdk.business.ecg_device_characterisitcs;

import android.support.annotation.NonNull;

import com.mircod.mircodandroidsdk.data.device_repository.ecg_holter.IEcgPatchRepository;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.internal.mircod.entities.base.ShortData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.EcgData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.OrientationData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.TemperatureData;

import java.util.List;
import java.util.UUID;

import rx.Observable;
import rx.functions.Func1;

public class EcgCharacteristicsInteractor implements IHolterCharacteristicsInteractor {


    private final IEcgPatchRepository mRepository;

    public EcgCharacteristicsInteractor(IEcgPatchRepository repository) {
        mRepository = repository;
    }

    @Override
    public Observable<EcgData> getEcgCharacteristicData(IBleDeviceCharacteristic characteristic) {
        return mRepository.getEcgCharacteristicData(characteristic);
    }

    @Override
    public Observable<ShortData> getCharacteristicData(IBleDeviceCharacteristic characteristic) {
        return mRepository.getCharacteristicData(characteristic);
    }

    @Override
    public Observable<OrientationData> getOrientationCharacteristicData(IBleDeviceCharacteristic characteristic) {
        return mRepository.getOrientationCharacteristicData(characteristic);
    }

    @Override
    public Observable<List<IBleDeviceCharacteristic>> getServiceCharacteristics(UUID serviceUUID) {
        return mRepository.getCharacteristics(serviceUUID);
    }

    @Override
    public Observable<BleDeviceService> getDeviceServiceByUUID(@NonNull UUID serviceUUID) {
        return mRepository.getDeviceServiceByUUID(serviceUUID);
    }

    @Override
    public void setDeviceMacAddress(String macAddress) {
        mRepository.setDeviceMacAddress(macAddress);
    }

    @Override
    public Observable<String> subscribeCharacteristic(IBleDeviceCharacteristic characteristic) {
        String uuid = characteristic.getUUID().toString();

        if (uuid.equals(generateUUID("ffe1"))) {
            return getEcgCharacteristicData(characteristic)
                    .map(new Func1<EcgData, String>() {
                        @Override
                        public String call(EcgData ecgData) {
                            return ecgData.toString();
                        }
                    });
        }
        if (uuid.equals(generateUUID("ffe7"))) {
            return getOrientationCharacteristicData(characteristic)
                    .map(new Func1<OrientationData, String>() {
                        @Override
                        public String call(OrientationData orientationData) {
                            return orientationData.toString();
                        }
                    });
        }
        if (uuid.equals(generateUUID("ffe6")) || uuid.equals(generateUUID("ffe4"))) {
            return mRepository.getTemperatureData(characteristic)
                    .map(new Func1<TemperatureData, String>() {
                        @Override
                        public String call(TemperatureData temperatureData) {
                            return temperatureData.toString();
                        }
                    });
        }


        return getCharacteristicData(characteristic).map(new Func1<ShortData, String>() {
            @Override
            public String call(ShortData shortData) {
                return shortData.toString();
            }
        });
    }

    private String generateUUID(String header) {
        return "0000" + header + "-0000-1000-8000-00805f9b34fb";
    }

}
