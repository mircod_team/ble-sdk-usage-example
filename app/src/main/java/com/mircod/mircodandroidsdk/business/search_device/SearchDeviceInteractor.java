package com.mircod.mircodandroidsdk.business.search_device;

import com.mircod.mircodandroidsdk.business.ScreenBridge;
import com.mircod.mircodandroidsdk.data.device_repository.search.ISearchDeviceRepository;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;
import com.mircod.mircodblesdk.entities.ble_device.IBleScanDevice;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Andrey on 20.03.2018.
 */

public class SearchDeviceInteractor implements ISearchDeviceInteractor {

    private final ISearchDeviceRepository mDeviceRepository;
    private final ScreenBridge mScreenBridge;

    public SearchDeviceInteractor(ISearchDeviceRepository deviceRepository, ScreenBridge screenBridge) {
        mDeviceRepository = deviceRepository;
        mScreenBridge = screenBridge;
    }

    @Override
    public Observable<BleDevice> startSearchDevices(int interval, TimeUnit timeUnit) {
        return mDeviceRepository.startDeviceScan(interval, timeUnit)
                .map(new Func1<IBleScanDevice, BleDevice>() {
                    @Override
                    public BleDevice call(IBleScanDevice bleDevice) {
                        return new BleDevice(bleDevice);
                    }
                });
    }

    @Override
    public void connectToDevice(BleDevice bleDevice) {
        mScreenBridge.addDevice(bleDevice);
    }
}
