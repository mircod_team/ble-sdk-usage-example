package com.mircod.mircodandroidsdk.business.device_characteristic;

import android.support.annotation.NonNull;

import com.mircod.mircodandroidsdk.data.device_repository.characteristics.IDeviceCharacteristicsRepository;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;

import java.util.List;
import java.util.UUID;

import rx.Observable;

/**
 * Created by Andrey on 27.03.2018.
 */

public class DeviceCharacteristicInteractor implements IDeviceCharacteristicInteractor {

    private final IDeviceCharacteristicsRepository mCharacteristicsRepository;

    public DeviceCharacteristicInteractor(IDeviceCharacteristicsRepository characteristicsRepository) {
        mCharacteristicsRepository = characteristicsRepository;
    }

    @Override
    public void setDeviceMacAddress(String macAddress) {
        mCharacteristicsRepository.setDeviceMacAddress(macAddress);
    }

    public Observable<byte[]> readCharacteristic(UUID uuid) {
        return mCharacteristicsRepository.readCharacteristic(uuid);
    }

    @Override
    public Observable<IBleDeviceCharacteristic> readCharacteristic(IBleDeviceCharacteristic characteristic) {
        return mCharacteristicsRepository.readCharacteristic(characteristic);
    }

    @Override
    public Observable<IBleDeviceCharacteristic> notifyCharacteristic(IBleDeviceCharacteristic characteristic) {
        return mCharacteristicsRepository.characteristicEnableNotification(characteristic);
    }

    @Override
    public Observable<List<IBleDeviceCharacteristic>> getServiceCharacteristics(UUID serviceUUID) {
        return mCharacteristicsRepository.getCharacteristics(serviceUUID);
    }

    @Override
    public Observable<BleDeviceService> getDeviceServiceByUUID(@NonNull final UUID serviceUUID) {
        return mCharacteristicsRepository.getDeviceServiceByUUID(serviceUUID);
    }

    @Override
    public Observable<byte[]> writeCharacteristic(IBleDeviceCharacteristic characteristic, byte[] data) {
        return mCharacteristicsRepository.writeToCharacteristic(characteristic, data);
    }
}
