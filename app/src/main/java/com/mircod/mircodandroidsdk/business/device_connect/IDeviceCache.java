package com.mircod.mircodandroidsdk.business.device_connect;

import android.support.annotation.NonNull;

import com.mircod.mircodblesdk.core.IBleDeviceConnection;

import org.jetbrains.annotations.NotNull;

/**
 * Created by Andrey on 3/25/2018.
 */

public interface IDeviceCache<T> {

    void addDevice(@NotNull T bleDevice);

    boolean removeDevice(@NotNull T bleDevice);

    T getDevice(@NotNull String mac);

    boolean contains(@NotNull String macAddress);

    void addDeviceConnection(String deviceMacAddress, IBleDeviceConnection bleDeviceConnection);

    IBleDeviceConnection getDeviceConnection(@NonNull String macAddress);
}
