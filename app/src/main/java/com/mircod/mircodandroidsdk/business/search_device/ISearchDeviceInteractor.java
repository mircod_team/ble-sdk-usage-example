package com.mircod.mircodandroidsdk.business.search_device;

import com.mircod.mircodandroidsdk.data.entityes.BleDevice;

import java.util.concurrent.TimeUnit;

import rx.Observable;

/**
 * Created by Andrey on 20.03.2018.
 */

public interface ISearchDeviceInteractor {

    Observable<BleDevice> startSearchDevices(int interval, TimeUnit timeUnit);

    void connectToDevice(BleDevice bleDevice);
}
