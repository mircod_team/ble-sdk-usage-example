package com.mircod.mircodandroidsdk.business.device_connect;

import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.entities.BleConnectionState;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import rx.Observable;

/**
 * Created by Andrey on 23.03.2018.
 */

public interface IDeviceConnectInteractor {
    BleDevice getBleDevice(@NotNull String macAddress);

    Observable<BleConnectionState> connect(BleDevice bleDevice);

    Observable<List<BleDeviceService>> getAllServices();

    Observable<BleConnectionState> getDeviceConnectionState(String macAddress);

    boolean deviceIsConnected(String macAddress);

}
