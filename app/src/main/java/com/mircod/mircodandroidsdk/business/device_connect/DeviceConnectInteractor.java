package com.mircod.mircodandroidsdk.business.device_connect;

import com.mircod.mircodandroidsdk.business.ScreenBridge;
import com.mircod.mircodandroidsdk.data.device_repository.connect.IDeviceConnectRepository;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.entities.BleConnectionState;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import rx.Observable;

/**
 * Created by Andrey on 23.03.2018.
 */

public class DeviceConnectInteractor implements IDeviceConnectInteractor {

    private final ScreenBridge mScreenBridge;
    private final IDeviceConnectRepository mConnectRepository;

    public DeviceConnectInteractor(ScreenBridge screenBridge, IDeviceConnectRepository connectRepository){
        mScreenBridge = screenBridge;
        mConnectRepository = connectRepository;
    }

    @Override
    public BleDevice getBleDevice(@NotNull String macAddress) {
        return mScreenBridge.getDevice(macAddress);
    }

    @Override
    public Observable<BleConnectionState> connect(BleDevice bleDevice) {
        return mConnectRepository.connectToDevice(bleDevice);
    }

    @Override
    public Observable<List<BleDeviceService>> getAllServices() {
        return mConnectRepository.getAllService();
    }

    @Override
    public Observable<BleConnectionState> getDeviceConnectionState(String macAddress) {
        return mConnectRepository.getDeviceConnectionState(macAddress);
    }

    @Override
    public boolean deviceIsConnected(String macAddress) {
        return mConnectRepository.deviceIsConnected(macAddress);
    }
}
