package com.mircod.mircodandroidsdk.business.ecg_device_characterisitcs;

import android.support.annotation.NonNull;

import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.internal.mircod.entities.base.ShortData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.EcgData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.OrientationData;

import java.util.List;
import java.util.UUID;

import rx.Observable;

public interface IHolterCharacteristicsInteractor {

    Observable<EcgData> getEcgCharacteristicData(IBleDeviceCharacteristic characteristic);

    Observable<ShortData> getCharacteristicData(IBleDeviceCharacteristic characteristic);

    Observable<OrientationData> getOrientationCharacteristicData(IBleDeviceCharacteristic characteristic);

    Observable<List<IBleDeviceCharacteristic>> getServiceCharacteristics(UUID serviceUUID);

    Observable<BleDeviceService> getDeviceServiceByUUID(@NonNull final UUID serviceUUID);

    void setDeviceMacAddress(String macAddress);

    Observable<String> subscribeCharacteristic(IBleDeviceCharacteristic characteristic);
}
