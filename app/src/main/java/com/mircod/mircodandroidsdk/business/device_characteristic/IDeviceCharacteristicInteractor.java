package com.mircod.mircodandroidsdk.business.device_characteristic;

import android.support.annotation.NonNull;

import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;

import java.util.List;
import java.util.UUID;

import rx.Observable;

/**
 * Created by Andrey on 27.03.2018.
 */

public interface IDeviceCharacteristicInteractor {

    void setDeviceMacAddress(String macAddress);

    Observable<List<IBleDeviceCharacteristic>> getServiceCharacteristics(UUID serviceUUID);

    Observable<BleDeviceService> getDeviceServiceByUUID(@NonNull final UUID serviceUUID);

    Observable<byte[]> readCharacteristic(UUID uuid);

    Observable<IBleDeviceCharacteristic> readCharacteristic(IBleDeviceCharacteristic characteristic);

    Observable<IBleDeviceCharacteristic> notifyCharacteristic(IBleDeviceCharacteristic characteristic);

    Observable<byte[]> writeCharacteristic(IBleDeviceCharacteristic characteristic, byte[] data);
}
