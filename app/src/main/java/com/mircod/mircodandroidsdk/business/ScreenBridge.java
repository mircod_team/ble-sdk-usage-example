package com.mircod.mircodandroidsdk.business;

import android.support.annotation.NonNull;

import com.mircod.mircodandroidsdk.data.entityes.BleDevice;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrey on 23.03.2018.
 */

public class ScreenBridge {

    private Map<String, BleDevice> mBleDevices = new HashMap<>();

    public void addDevice(@NonNull BleDevice device) {
        mBleDevices.put(device.getMacAddress().toLowerCase(), device);
    }

    public BleDevice getDevice(@NotNull String macAddress) {
        return mBleDevices.get(macAddress.toLowerCase());
    }
}
