package com.mircod.mircodandroidsdk.business.device_connect;

import android.support.annotation.NonNull;
import android.util.Log;

import com.mircod.mircodblesdk.core.IBleDeviceConnection;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Andrey on 3/25/2018.
 */

public class DeviceCache implements IDeviceCache<IBleDevice> {

    private Map<String, IBleDevice> mBleDeviceCache = new HashMap<>();
    private Map<String, IBleDeviceConnection> mBleDeviceConnectionMap = new HashMap<>();

    public DeviceCache() {
        Log.d("DeviceCache", "create ");
    }

    @Override
    public void addDevice(@NotNull IBleDevice bleDevice) {
        mBleDeviceCache.put(bleDevice.getDeviceMacAddress().toLowerCase(), bleDevice);
    }

    @Override
    public boolean removeDevice(@NotNull IBleDevice bleDevice) {
        return mBleDeviceCache.remove(bleDevice.getDeviceMacAddress().toLowerCase()) != null;
    }

    @Override
    public IBleDevice getDevice(@NotNull String mac) {
        return mBleDeviceCache.get(mac.toLowerCase());
    }

    @Override
    public boolean contains(@NotNull String macAddress) {
        return mBleDeviceCache.containsKey(macAddress.toLowerCase());
    }

    @Override
    public void addDeviceConnection(String deviceMacAddress, IBleDeviceConnection bleDeviceConnection) {
        mBleDeviceConnectionMap.put(deviceMacAddress.toLowerCase(), bleDeviceConnection);
    }

    @Override
    public IBleDeviceConnection getDeviceConnection(@NonNull String macAddress) {
        return mBleDeviceConnectionMap.get(macAddress.toLowerCase());
    }
}
