package com.mircod.mircodandroidsdk.data.device_repository.search;


import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;
import com.mircod.mircodblesdk.entities.ble_device.IBleScanDevice;

import java.util.concurrent.TimeUnit;

import rx.Observable;

/**
 * Created by Andrey on 20.03.2018.
 */

public interface ISearchDeviceRepository {

    Observable<IBleScanDevice> startDeviceScan(int interval, TimeUnit timeUnit);

}
