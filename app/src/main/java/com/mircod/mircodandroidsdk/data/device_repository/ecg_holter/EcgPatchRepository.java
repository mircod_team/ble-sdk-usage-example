package com.mircod.mircodandroidsdk.data.device_repository.ecg_holter;

import com.mircod.mircodandroidsdk.business.device_connect.IDeviceCache;
import com.mircod.mircodandroidsdk.data.device_repository.characteristics.IDeviceCharacteristicsRepository;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.internal.mircod.MircodCharacteristicData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.base.ShortData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.EcgData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.OrientationData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.TemperatureData;
import com.mircod.mircodblesdk.core.internal.mircod.parsers.CastFunc;
import com.mircod.mircodblesdk.core.internal.mircod.parsers.ecg.EcgParser;
import com.mircod.mircodblesdk.core.internal.mircod.parsers.ecg_patch.OrientationParser;
import com.mircod.mircodblesdk.core.internal.mircod.parsers.ecg_patch.ShortParser;
import com.mircod.mircodblesdk.core.internal.mircod.parsers.ecg_patch.TemperatureParser;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;

import java.util.List;
import java.util.UUID;

import rx.Observable;

public class EcgPatchRepository implements IEcgPatchRepository {

    private final IDeviceCache<IBleDevice> mDeviceCache;
    private final IDeviceCharacteristicsRepository mDeviceCharacteristicsRepository;
    private String mMacAddress;

    public EcgPatchRepository(IDeviceCache<IBleDevice> deviceCache, IDeviceCharacteristicsRepository deviceCharacteristicsRepository) {
        mDeviceCache = deviceCache;
        mDeviceCharacteristicsRepository = deviceCharacteristicsRepository;
    }

    @Override
    public void setDeviceMacAddress(String macAddress) {
        mMacAddress = macAddress;
        mDeviceCharacteristicsRepository.setDeviceMacAddress(macAddress);
    }

    @Override
    public Observable<byte[]> readCharacteristic(UUID uuid) {
        return mDeviceCharacteristicsRepository.readCharacteristic(uuid);
    }

    @Override
    public Observable<IBleDeviceCharacteristic> readCharacteristic(IBleDeviceCharacteristic characteristic) {
        return mDeviceCharacteristicsRepository.readCharacteristic(characteristic);
    }

    @Override
    public Observable<List<IBleDeviceCharacteristic>> getCharacteristics(UUID serviceUUID) {
        return mDeviceCharacteristicsRepository.getCharacteristics(serviceUUID);
    }

    @Override
    public Observable<BleDeviceService> getDeviceServiceByUUID(UUID serviceUUID) {
        return mDeviceCharacteristicsRepository.getDeviceServiceByUUID(serviceUUID);
    }

    @Override
    public Observable<IBleDeviceCharacteristic> characteristicEnableNotification(IBleDeviceCharacteristic characteristic) {
        return null;
    }

    @Override
    public Observable<byte[]> writeToCharacteristic(IBleDeviceCharacteristic characteristic, byte[] data) {
        return null;
    }

    @Override
    public Observable<EcgData> getEcgCharacteristicData(IBleDeviceCharacteristic characteristic) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .mircodCharacteristicSubscribe(characteristic, new EcgParser(), new CastFunc<MircodCharacteristicData, EcgData>() {
                    @Override
                    public EcgData call(MircodCharacteristicData mircodCharacteristicData) {
                        return (EcgData) mircodCharacteristicData;
                    }
                });
    }

    @Override
    public Observable<ShortData> getCharacteristicData(IBleDeviceCharacteristic characteristic) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .mircodCharacteristicSubscribe(characteristic, new ShortParser(), new CastFunc<MircodCharacteristicData, ShortData>() {
                    @Override
                    public ShortData call(MircodCharacteristicData mircodCharacteristicData) {
                        return (ShortData) mircodCharacteristicData;
                    }
                });
    }

    @Override
    public Observable<OrientationData> getOrientationCharacteristicData(IBleDeviceCharacteristic characteristic) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .mircodCharacteristicSubscribe(characteristic, new OrientationParser(), new CastFunc<MircodCharacteristicData, OrientationData>() {
                    @Override
                    public OrientationData call(MircodCharacteristicData mircodCharacteristicData) {
                        return (OrientationData) mircodCharacteristicData;
                    }
                });
    }

    @Override
    public Observable<TemperatureData> getTemperatureData(IBleDeviceCharacteristic characteristic) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .mircodCharacteristicSubscribe(characteristic, new TemperatureParser(), new CastFunc<MircodCharacteristicData, TemperatureData>() {
                    @Override
                    public TemperatureData call(MircodCharacteristicData mircodCharacteristicData) {
                        return (TemperatureData) mircodCharacteristicData;
                    }
                });
    }
}
