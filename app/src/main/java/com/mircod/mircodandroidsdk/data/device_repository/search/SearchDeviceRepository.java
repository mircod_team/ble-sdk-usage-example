package com.mircod.mircodandroidsdk.data.device_repository.search;

import com.mircod.mircodblesdk.client.IMircodSdkClient;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;
import com.mircod.mircodblesdk.entities.ble_device.IBleScanDevice;
import com.mircod.mircodblesdk.scanner.ScanSettings;

import java.util.concurrent.TimeUnit;

import rx.Observable;

/**
 * Created by Andrey on 20.03.2018.
 */

public class SearchDeviceRepository implements ISearchDeviceRepository {

    private final IMircodSdkClient mMircodSdkClient;

    public SearchDeviceRepository(IMircodSdkClient mircodSdkClient) {
        mMircodSdkClient = mircodSdkClient;
    }

    @Override
    public Observable<IBleScanDevice> startDeviceScan(int interval, TimeUnit timeUnit) {
        return mMircodSdkClient.scanDevices(new ScanSettings.Builder().setScanPeriod(10).build());
    }

}
