package com.mircod.mircodandroidsdk.data.device_repository.characteristics;

import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;

import java.util.List;
import java.util.UUID;

import rx.Observable;

/**
 * Created by Andrey on 27.03.2018.
 */

public interface IDeviceCharacteristicsRepository {

    void setDeviceMacAddress(String macAddress);

    Observable<byte[]> readCharacteristic(UUID uuid);

    Observable<IBleDeviceCharacteristic> readCharacteristic(IBleDeviceCharacteristic characteristic);

    Observable<List<IBleDeviceCharacteristic>> getCharacteristics(final UUID serviceUUID);

    Observable<BleDeviceService> getDeviceServiceByUUID(final UUID serviceUUID);

    Observable<IBleDeviceCharacteristic> characteristicEnableNotification(IBleDeviceCharacteristic characteristic);

    Observable<byte[]> writeToCharacteristic(IBleDeviceCharacteristic characteristic, byte[] data);
}
