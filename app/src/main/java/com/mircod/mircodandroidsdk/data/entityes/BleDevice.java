package com.mircod.mircodandroidsdk.data.entityes;

import android.bluetooth.le.ScanRecord;

import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;
import com.mircod.mircodblesdk.entities.ble_device.IBleScanDevice;
import com.mircod.mircodblesdk.utils.BytesConverterHelper;

/**
 * Created by Andrey on 21.03.2018.
 */

public class BleDevice {

    private final IBleScanDevice mBleDevice;

    public BleDevice(IBleScanDevice bleDevice){
        mBleDevice = bleDevice;
    }

    public String getName(){
        return mBleDevice.getDeviceName() == null ? "Unknown device" : mBleDevice.getDeviceName();
    }

    public String getMacAddress(){
        return mBleDevice.getDeviceMacAddress();
    }

    public boolean isBounded(){
        return mBleDevice.isBound();
    }

    public String getBoundedState(){
        return isBounded() ? "BOUNDED" : "NOT BOUNDED";
    }

    public String getManufacturerData(){
        ScanRecord scanRecord = getScanRecord();
        if (scanRecord != null && scanRecord.getManufacturerSpecificData() != null){
            byte[] bytes = scanRecord.getManufacturerSpecificData().valueAt(0);
            if (bytes != null)
            return "0x"+ BytesConverterHelper.bytesToHex(bytes);
        }
        return "not found";
    }

    public String getUUIDString(){
        ScanRecord scanRecord = getScanRecord();
        String prefix = "Service UUID: ";
        if (scanRecord != null && scanRecord.getServiceUuids() != null && scanRecord.getServiceUuids().size() != 0){
            return prefix.concat(scanRecord.getServiceUuids().get(0).toString().split("-")[0]);
        }
        return prefix.concat("not initialized");
    }

    private ScanRecord getScanRecord(){
        return mBleDevice.getScanRecord();
    }

    @Override
    public String toString() {
        return mBleDevice.toString();
    }

    public IBleDevice getIBleDevice() {
        return mBleDevice;
    }
}
