package com.mircod.mircodandroidsdk.data.device_repository.connect;

import android.util.Log;

import com.mircod.mircodandroidsdk.business.device_connect.IDeviceCache;
import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodblesdk.core.IBleDeviceConnection;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceServices;
import com.mircod.mircodblesdk.entities.BleConnectionState;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;
import com.mircod.mircodblesdk.utils.ConnectionSettings;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Andrey on 23.03.2018.
 */

public class DeviceConnectRepository implements IDeviceConnectRepository {

    private IBleDeviceConnection mBleDeviceConnection;
    private IDeviceCache<IBleDevice> mDeviceCache;
    private final String TAG = DeviceConnectRepository.class.getSimpleName();
    private final CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    public DeviceConnectRepository(IDeviceCache<IBleDevice> deviceCache) {
        mDeviceCache = deviceCache;
    }

    @Override
    public Observable<BleConnectionState> connectToDevice(final BleDevice bleDevice) {
        final IBleDevice iBleDevice = bleDevice.getIBleDevice();
        mDeviceCache.addDevice(iBleDevice);
        iBleDevice.connect(new ConnectionSettings.Builder().build())
                .subscribe(new Action1<IBleDeviceConnection>() {
                    @Override
                    public void call(IBleDeviceConnection bleDeviceConnection) {
                        Log.d(TAG, "IBleDeviceConnection");
                        mBleDeviceConnection = bleDeviceConnection;
                        mDeviceCache.addDeviceConnection(iBleDevice.getDeviceMacAddress(), bleDeviceConnection);
                    }
                });


        return iBleDevice.observeConnectionState()
                .debounce(1, TimeUnit.SECONDS)
                .doOnNext(new Action1<BleConnectionState>() {
                    @Override
                    public void call(BleConnectionState bleConnectionState) {
                        Log.d(TAG, "ble connection state: " + bleConnectionState);
                    }
                });
    }

    @Override
    public Observable<BleConnectionState> getDeviceConnectionState(String mac) {
        return mDeviceCache.getDevice(mac).observeConnectionState();
    }

    @Override
    public Observable<List<BleDeviceService>> getAllService() {
        Log.d(TAG, "getAllService: ");
        return mBleDeviceConnection.getBleDeviceServices()
                .map(new Func1<BleDeviceServices, List<BleDeviceService>>() {
                    @Override
                    public List<BleDeviceService> call(BleDeviceServices bleDeviceServices) {
                        Log.d(TAG, "getAllService->call: ");
                        List<BleDeviceService> deviceServices = bleDeviceServices.getDeviceServices();
                        for (BleDeviceService deviceService : deviceServices) {
                            System.out.println("DeviceService: " + deviceService.getServiceUUID());
                        }
                        return deviceServices;
                    }
                });
    }

    @Override
    public boolean deviceIsConnected(String macAddress) {
        if (mDeviceCache.contains(macAddress)){
            final BleConnectionState[] deviceConnectionState = new BleConnectionState[1];
            mDeviceCache.getDevice(macAddress.toLowerCase())
                    .observeConnectionState()
                    .subscribe(new Action1<BleConnectionState>() {
                        @Override
                        public void call(BleConnectionState bleConnectionState) {
                            deviceConnectionState[0] = bleConnectionState;
                        }
                    });
            return deviceConnectionState[0] == BleConnectionState.CONNECTED;
        }else{
            return false;
        }
    }


}
