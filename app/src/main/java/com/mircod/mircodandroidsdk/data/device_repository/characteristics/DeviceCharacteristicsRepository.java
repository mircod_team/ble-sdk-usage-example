package com.mircod.mircodandroidsdk.data.device_repository.characteristics;

import android.support.annotation.NonNull;

import com.mircod.mircodandroidsdk.business.device_connect.IDeviceCache;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceServices;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;

import java.util.List;
import java.util.UUID;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Andrey on 27.03.2018.
 */

public class DeviceCharacteristicsRepository implements IDeviceCharacteristicsRepository {

    private final IDeviceCache<IBleDevice> mDeviceCache;
    private String mMacAddress;

    public DeviceCharacteristicsRepository(IDeviceCache<IBleDevice> deviceCache) {
        mDeviceCache = deviceCache;
    }

    @Override
    public void setDeviceMacAddress(String macAddress) {
        mMacAddress = macAddress;
    }

    @Override
    public Observable<byte[]> readCharacteristic(UUID uuid) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .readCharacteristic(uuid);
    }

    @Override
    public Observable<IBleDeviceCharacteristic> readCharacteristic(IBleDeviceCharacteristic characteristic) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .readCharacteristic(characteristic);
    }

    public Observable<List<IBleDeviceCharacteristic>> getCharacteristics(final UUID serviceUUID) {
        return getDeviceService(serviceUUID)
                .map(new Func1<BleDeviceService, List<IBleDeviceCharacteristic>>() {
                    @Override
                    public List<IBleDeviceCharacteristic> call(BleDeviceService bleDeviceService) {
                        return bleDeviceService.getBleDeviceCharacteristics();
                    }
                });
    }

    public Observable<BleDeviceService> getDeviceServiceByUUID(final UUID serviceUUID) {
        return getDeviceService(serviceUUID);
    }

    @NonNull
    private Observable<BleDeviceService> getDeviceService(final UUID serviceUUID) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .getBleDeviceServices()
                .flatMap(new Func1<BleDeviceServices, Observable<BleDeviceService>>() {
                    @Override
                    public Observable<BleDeviceService> call(BleDeviceServices bleDeviceServices) {
                        return Observable.from(bleDeviceServices.getDeviceServices());
                    }
                })
                .filter(new Func1<BleDeviceService, Boolean>() {
                    @Override
                    public Boolean call(BleDeviceService bleDeviceService) {
                        return bleDeviceService.getServiceUUID().equals(serviceUUID);
                    }
                });
    }

    @Override
    public Observable<IBleDeviceCharacteristic> characteristicEnableNotification(IBleDeviceCharacteristic characteristic) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .subscribeCharacteristicNotification(characteristic);
    }

    @Override
    public Observable<byte[]> writeToCharacteristic(IBleDeviceCharacteristic characteristic, byte[] data) {
        return mDeviceCache.getDeviceConnection(mMacAddress)
                .writeToCharacteristic(characteristic.getUUID(), data);
    }
}
