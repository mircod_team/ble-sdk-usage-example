package com.mircod.mircodandroidsdk.data.device_repository.ecg_holter;

import com.mircod.mircodandroidsdk.data.device_repository.characteristics.IDeviceCharacteristicsRepository;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.interfaces.IBleDeviceCharacteristic;
import com.mircod.mircodblesdk.core.internal.mircod.entities.base.ShortData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.EcgData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.OrientationData;
import com.mircod.mircodblesdk.core.internal.mircod.entities.ecg_patch.TemperatureData;

import rx.Observable;

public interface IEcgPatchRepository extends IDeviceCharacteristicsRepository {

    Observable<EcgData> getEcgCharacteristicData(IBleDeviceCharacteristic characteristic);

    Observable<ShortData> getCharacteristicData(IBleDeviceCharacteristic characteristic);

    Observable<OrientationData> getOrientationCharacteristicData(IBleDeviceCharacteristic characteristic);

    Observable<TemperatureData> getTemperatureData(IBleDeviceCharacteristic characteristic);

}
