package com.mircod.mircodandroidsdk.data.device_repository.connect;

import com.mircod.mircodandroidsdk.data.entityes.BleDevice;
import com.mircod.mircodblesdk.core.ble_device_interaction.entity.BleDeviceService;
import com.mircod.mircodblesdk.entities.BleConnectionState;

import java.util.List;

import rx.Observable;


/**
 * Created by Andrey on 23.03.2018.
 */

public interface IDeviceConnectRepository {

    Observable<BleConnectionState> connectToDevice(BleDevice bleDevice);

    Observable<BleConnectionState> getDeviceConnectionState(String mac);

    Observable<List<BleDeviceService>> getAllService();

    boolean deviceIsConnected(String macAddress);

}
