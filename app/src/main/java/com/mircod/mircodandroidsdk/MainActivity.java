package com.mircod.mircodandroidsdk;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.mircod.mircodandroidsdk.ui.search_device.fmts.SearchDevicesFragment;
import com.mircod.mircodandroidsdk.ui.search_device.fmts.ServicesListFragment;
import com.mircod.mircodblesdk.core.IBleDeviceConnection;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private IBleDeviceConnection mBleDeviceConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.searchFmtContainer, new SearchDevicesFragment(), "")
                .commit();
    }

    public void connectToDevice(IBleDevice bleScanDevice) {
        Log.d(TAG, "connectToDevice: " + bleScanDevice);

        ServicesListFragment servicesListFragment = new ServicesListFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("device", bleScanDevice);
        servicesListFragment.setArguments(bundle);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.connectedDevice, servicesListFragment)
                .commit();

    }


}
