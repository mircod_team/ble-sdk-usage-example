package com.mircod.mircodandroidsdk.di.device_connection;

import com.mircod.mircodandroidsdk.business.ScreenBridge;
import com.mircod.mircodandroidsdk.business.device_connect.DeviceConnectInteractor;
import com.mircod.mircodandroidsdk.business.device_connect.IDeviceConnectInteractor;
import com.mircod.mircodandroidsdk.data.device_repository.connect.IDeviceConnectRepository;
import com.mircod.mircodandroidsdk.ui.connection_screen.presenter.DeviceConnectPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andrey on 23.03.2018.
 */

@Module
public class DeviceConnectModule {

    @Provides
    DeviceConnectPresenter provideDeviceConnectPresenter() {
        return new DeviceConnectPresenter();
    }

    @Provides
    IDeviceConnectInteractor provideDeviceConnectInteractor(ScreenBridge screenBridge, IDeviceConnectRepository connectRepository) {
        return new DeviceConnectInteractor(screenBridge, connectRepository);
    }

}
