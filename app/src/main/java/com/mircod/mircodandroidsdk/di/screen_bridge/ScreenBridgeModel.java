package com.mircod.mircodandroidsdk.di.screen_bridge;

import com.mircod.mircodandroidsdk.business.ScreenBridge;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andrey on 23.03.2018.
 */
@Module
public class ScreenBridgeModel {

    @Provides
    @Singleton
    ScreenBridge provideDeviceBridge(){
        return new ScreenBridge();
    }

}
