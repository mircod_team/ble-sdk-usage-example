package com.mircod.mircodandroidsdk.di.device_characteristic;

import com.mircod.mircodandroidsdk.business.device_characteristic.DeviceCharacteristicInteractor;
import com.mircod.mircodandroidsdk.business.device_characteristic.IDeviceCharacteristicInteractor;
import com.mircod.mircodandroidsdk.business.device_connect.IDeviceCache;
import com.mircod.mircodandroidsdk.data.device_repository.characteristics.DeviceCharacteristicsRepository;
import com.mircod.mircodandroidsdk.data.device_repository.characteristics.IDeviceCharacteristicsRepository;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andrey on 28.03.2018.
 */
@Module
public class DeviceCharacteristicModule {

    @Provides
    IDeviceCharacteristicInteractor proviceCharacteristicInteractor(IDeviceCharacteristicsRepository characteristicsRepository) {
        return new DeviceCharacteristicInteractor(characteristicsRepository);
    }

    @Provides
    IDeviceCharacteristicsRepository provideCharacteristicsRepository(IDeviceCache<IBleDevice> deviceCache) {
        return new DeviceCharacteristicsRepository(deviceCache);
    }
}
