package com.mircod.mircodandroidsdk.di.holter_characteristics;

import com.mircod.mircodandroidsdk.business.device_connect.IDeviceCache;
import com.mircod.mircodandroidsdk.business.ecg_device_characterisitcs.EcgCharacteristicsInteractor;
import com.mircod.mircodandroidsdk.business.ecg_device_characterisitcs.IHolterCharacteristicsInteractor;
import com.mircod.mircodandroidsdk.data.device_repository.characteristics.DeviceCharacteristicsRepository;
import com.mircod.mircodandroidsdk.data.device_repository.characteristics.IDeviceCharacteristicsRepository;
import com.mircod.mircodandroidsdk.data.device_repository.ecg_holter.EcgPatchRepository;
import com.mircod.mircodandroidsdk.data.device_repository.ecg_holter.IEcgPatchRepository;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;

import dagger.Module;
import dagger.Provides;

@Module
public class EcgPatchDeviceModule {

    @Provides
    IHolterCharacteristicsInteractor providePresenter(IEcgPatchRepository repository){
        return new EcgCharacteristicsInteractor(repository);
    }

    @Provides
    IEcgPatchRepository provideEcgPatchRepository(IDeviceCache<IBleDevice> deviceCache, IDeviceCharacteristicsRepository characteristicsRepository){
        return new EcgPatchRepository(deviceCache, characteristicsRepository);
    }

    @Provides
    IDeviceCharacteristicsRepository provideCharacteristicsRepository(IDeviceCache<IBleDevice> deviceCache){
        return new DeviceCharacteristicsRepository(deviceCache);
    }

}
