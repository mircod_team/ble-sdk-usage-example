package com.mircod.mircodandroidsdk.di.device_characteristic;

import com.mircod.mircodandroidsdk.ui.characteristics_viewer.common.presenter.CharacteristicViewerPresenter;

import dagger.Subcomponent;

/**
 * Created by Andrey on 28.03.2018.
 */
@Subcomponent(modules = DeviceCharacteristicModule.class)
public interface DeviceCharacteristicComponent {
    void inject(CharacteristicViewerPresenter presenter);
}
