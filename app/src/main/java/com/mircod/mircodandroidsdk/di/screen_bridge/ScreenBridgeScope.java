package com.mircod.mircodandroidsdk.di.screen_bridge;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Andrey on 23.03.2018.
 */
@Scope
@Retention(RUNTIME)
public @interface ScreenBridgeScope {
}
