package com.mircod.mircodandroidsdk.di.holter_characteristics;

import com.mircod.mircodandroidsdk.ui.characteristics_viewer.ecg.presenter.EcgPatchCharacteristicsViewerPresenter;

import dagger.Subcomponent;

@Subcomponent(modules = EcgPatchDeviceModule.class)
public interface EcgPatchDeviceComponent {
    void inject(EcgPatchCharacteristicsViewerPresenter presenter);
}
