package com.mircod.mircodandroidsdk.di.screen_bridge;

import dagger.Subcomponent;

/**
 * Created by Andrey on 23.03.2018.
 */
@ScreenBridgeScope
@Subcomponent(modules = ScreenBridgeModel.class)
public interface ScreenBridgeComponent {
    @Subcomponent.Builder
    interface Builder {
        ScreenBridgeComponent build();
    }
}
