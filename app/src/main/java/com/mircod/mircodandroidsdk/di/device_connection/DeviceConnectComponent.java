package com.mircod.mircodandroidsdk.di.device_connection;

import com.mircod.mircodandroidsdk.ui.connection_screen.presenter.DeviceConnectPresenter;

import dagger.Subcomponent;

/**
 * Created by Andrey on 23.03.2018.
 */
@Subcomponent(modules = {DeviceConnectModule.class})
@DeviceConnectionScope
public interface DeviceConnectComponent {
    void inject(DeviceConnectPresenter deviceConnectPresenter);
}