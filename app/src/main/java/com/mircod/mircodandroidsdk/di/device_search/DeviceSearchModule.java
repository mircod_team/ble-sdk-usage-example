package com.mircod.mircodandroidsdk.di.device_search;

import com.mircod.mircodandroidsdk.business.ScreenBridge;
import com.mircod.mircodandroidsdk.business.search_device.ISearchDeviceInteractor;
import com.mircod.mircodandroidsdk.business.search_device.SearchDeviceInteractor;
import com.mircod.mircodandroidsdk.data.device_repository.search.ISearchDeviceRepository;
import com.mircod.mircodandroidsdk.data.device_repository.search.SearchDeviceRepository;
import com.mircod.mircodblesdk.client.IMircodSdkClient;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andrey on 20.03.2018.
 */
@Module
public class DeviceSearchModule {

    @Provides
    ISearchDeviceInteractor provideSearchDeviceInteractor(ISearchDeviceRepository deviceRepository, ScreenBridge screenBridge){
        return new SearchDeviceInteractor(deviceRepository, screenBridge);
    }

    @Provides
    ISearchDeviceRepository provideSearchDeviceRepository(IMircodSdkClient mircodSdkClient){
        return new SearchDeviceRepository(mircodSdkClient);
    }



}
