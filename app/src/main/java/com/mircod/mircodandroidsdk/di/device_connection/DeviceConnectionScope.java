package com.mircod.mircodandroidsdk.di.device_connection;

import java.lang.annotation.Retention;

import javax.inject.Scope;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Andrey on 3/25/2018.
 */
@Scope
@Retention(RUNTIME)
public @interface DeviceConnectionScope {
}
