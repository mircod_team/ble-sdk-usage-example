package com.mircod.mircodandroidsdk.di.aplication;

import android.content.Context;

import com.mircod.mircodandroidsdk.business.device_connect.DeviceCache;
import com.mircod.mircodandroidsdk.business.device_connect.IDeviceCache;
import com.mircod.mircodandroidsdk.data.device_repository.connect.DeviceConnectRepository;
import com.mircod.mircodandroidsdk.data.device_repository.connect.IDeviceConnectRepository;
import com.mircod.mircodblesdk.client.IMircodSdkClient;
import com.mircod.mircodblesdk.client.MircodSdkClient;
import com.mircod.mircodblesdk.entities.ble_device.IBleDevice;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Andrey on 20.03.2018.
 */
@Module
public class AppModule {

    private final Context mContext;

    public AppModule(Context context){
        mContext = context;
    }

    @Provides
    Context provideContext(){
        return mContext;
    }

    @Provides
    IMircodSdkClient provideMircodSdkClient(){
        return MircodSdkClient.create(mContext);
    }

    @Provides
    @Singleton
    IDeviceCache<IBleDevice> provideDeviceCache() {
        return new DeviceCache();
    }

    @Provides
    @Singleton
    IDeviceConnectRepository provideDeviceConnectRepository(IDeviceCache<IBleDevice> deviceCache) {
        return new DeviceConnectRepository(deviceCache);
    }

}
