package com.mircod.mircodandroidsdk.di.aplication;

import com.mircod.mircodandroidsdk.di.device_characteristic.DeviceCharacteristicComponent;
import com.mircod.mircodandroidsdk.di.device_characteristic.DeviceCharacteristicModule;
import com.mircod.mircodandroidsdk.di.device_connection.DeviceConnectComponent;
import com.mircod.mircodandroidsdk.di.device_connection.DeviceConnectModule;
import com.mircod.mircodandroidsdk.di.device_search.DeviceSearchComponent;
import com.mircod.mircodandroidsdk.di.device_search.DeviceSearchModule;
import com.mircod.mircodandroidsdk.di.holter_characteristics.EcgPatchDeviceComponent;
import com.mircod.mircodandroidsdk.di.holter_characteristics.EcgPatchDeviceModule;
import com.mircod.mircodandroidsdk.di.screen_bridge.ScreenBridgeModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Andrey on 20.03.2018.
 */
@Component(modules = {AppModule.class, ScreenBridgeModel.class})
@Singleton
public interface AppComponent {

    DeviceSearchComponent plus(DeviceSearchModule module);
    DeviceConnectComponent plus(DeviceConnectModule deviceConnectModule);
    DeviceCharacteristicComponent plus(DeviceCharacteristicModule deviceCharacteristicModule);
    EcgPatchDeviceComponent plus(EcgPatchDeviceModule patchDeviceModule);

}
