package com.mircod.mircodandroidsdk.di.device_search;

import com.mircod.mircodandroidsdk.ui.search_device_screen.presenter.SearchDevicePresenter;

import dagger.Subcomponent;

/**
 * Created by Andrey on 20.03.2018.
 */
@Subcomponent(modules = DeviceSearchModule.class)
public interface DeviceSearchComponent {
    void inject(SearchDevicePresenter presenter);
}
